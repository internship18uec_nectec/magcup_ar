#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include <opencv2/opencv.hpp>
#include "cv_bridge/cv_bridge.h"


static const std::string OPENCV_WINDOW = "Image window";

void showimCallback (const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    cv_bridge::CvImagePtr cv_ptr;
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(1);
    ROS_INFO("Image show f [%d]", msg->header.seq);
  } catch (cv_bridge::Exception &e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rs435_listener");
  ros::NodeHandle n;
  cv::namedWindow(OPENCV_WINDOW);
  cv::startWindowThread();
  image_transport::ImageTransport it(n);
  image_transport::Subscriber image_sub = it.subscribe("/camera/color/image_raw", 1, showimCallback);
  ROS_INFO("test");
  ros::spin();

  cv::destroyWindow(OPENCV_WINDOW);

  return 0;
}
